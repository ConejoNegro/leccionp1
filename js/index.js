
$(function() {
    $('[data-toggle="tooltip"').tooltip();

    $('.carousel').carousel({
        interval: 2000
    });
    
    $('#contacto').on('show.bs.modal', function (e) {
            console.log('el modal se muestra.');

            $('#contactoBtn').removeClass('btn-outline-success');
            $('#contactoBtn').addClass('btn-primary');
            $('#contactoBtn').prop('disabled', true);



    });

    $('#contacto').on('shown.bs.modal', function (e) {
            console.log('el modal se cerró.');
    });


    $('#contacto').on('hide.bs.modal', function (e) {
            console.log('el modal se oculta.');
    });

    $('#contacto').on('hidden.bs.modal', function (e) {
            console.log('el modal se ocultó.');
            $('#contactoBtn').prop('disabled', false);
            $('#contactoBtn').addClass('btn-outline-success');
            $('#contactoBtn').removeClass('btn-primary');
    });
});
